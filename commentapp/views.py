from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login as loginn, authenticate, logout
from django.contrib.auth.forms import UserCreationForm
import json
from .models import User
from .forms import SignUpForm, passforget
import xlsxwriter
import mimetypes
import os
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import CommentForm
from . import sele



def forget(request):
    if request.method == 'POST':
        form = passforget(request.POST)
        if form.is_valid():
            answer = form.cleaned_data.get('answer')
            answer2 = form.cleaned_data.get('answer2')
            if request.user.questions_secret == answer:
                u = User.objects.get(username=request.user.username)
                u.set_password(answer2)
                messages.success(request, 'پسوورد تغییر کرد')
                return redirect('home')
            else:
                messages.error(request, 'پسوورد اشتباه می باشد')
            
    else:
        form = passforget()
    
    return render(request, 'commentapp/proj/forget pass.html', {'form': form})
    







def CommentFormPost(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = CommentForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            new_comment = form.save()
            new_comment.save()
            return HttpResponse('thanks for your comment')

    else:
        form = CommentForm()

    return render(request, 'commentapp/proj/home.html', {'form': form})


def tools(request):
    return render(request, 'commentapp/proj/tools.html')

def login(request):
    return render(request, 'commentapp/proj/login.html')



def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            questions_secret = form.cleaned_data.get('questions_secret')
            user = authenticate(username=username, password=raw_password, questions_secret = questions_secret)
            loginn(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'commentapp/proj/sing up.html', {'form': form})




@login_required
def Edu(request):
    return render(request, 'commentapp/proj/Educational concepts.html')



def search(request):
    q = request.GET.get('q')
    sele.search(q)                                    
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    fl = open(BASE_DIR + '/commentapp/'+'respnse.xlsx', 'rb').read()
    filename = 'respnse.xlsx'
    mime_type, _ = mimetypes.guess_type(BASE_DIR + '/commentapp/'+'respnse.xlsx')
    response = HttpResponse(fl, content_type=mime_type)
    response['Content-Disposition'] = "attachment; filename=%s" % filename
    return response

    ####  دانلود فایل اکسل و قسمت سرچ
    # except:
        # return HttpResponse('this isnt a valid value for search try again')



def download_file(request):
    # fill these variables with real values
    fl_path = ''
    filename = 'downloaded_file_name.extension'

    fl = open(fl_path, 'r')
    mime_type, _ = mimetypes.guess_type(fl_path)
    response = HttpResponse(fl, content_type=mime_type)
    response['Content-Disposition'] = "attachment; filename=%s" % filename
    return response

def article(request):
    return render(request, 'commentapp/proj/Articles.html')

def member(request):
    return render(request, 'commentapp/proj/members.html')

def data(request):
    return render(request, 'commentapp/proj/data.html')

def project(request):
    return render(request, 'commentapp/proj/projects.html')

def contactus(request):
    return render(request, 'commentapp/proj/contact-us.html')





def logout_view(request):
    logout(request)
    return redirect('home')
from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView

from .views import CommentFormPost, tools, forget, signup, login, Edu, search , article , data , member , project , contactus, logout_view

urlpatterns = [
    path('', CommentFormPost, name='home'),
    path('tools/', tools, name = 'tools'),
    path('forget/', forget, name = 'forget-pass'),
    path('signup/', signup, name = 'signup'),
    path('Educational/', Edu, name = 'Educational-concepts'),
    path('login/', LoginView.as_view(template_name = 'commentapp/proj/login.html'), name='login'),
    path('logout/', logout_view, name = 'logout'),
    path('search/', search, name = 'search_results'),
    path('articles/', article, name = 'articles'),
    path('data/', data, name = 'data'),
    path('members/', member, name = 'members'),
    path('projects/', project, name = 'projects'),
    path('contact-us/', contactus, name = 'contact-us'),
]
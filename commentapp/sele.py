from selenium import webdriver
import xlsxwriter
# from selenium.webdriver.common.keys import keys           #ایمپورت ماژول ها
import os
from time import sleep
import json




PATH = os.path.dirname(os.path.abspath(__file__)) #
PATH2 = os.path.join(PATH, 'chromedriver')
####################################  پیدا کردن فایل جیسون در پروژه
def find_file():                   
    keyword = '.json'              
    for fname in os.listdir(PATH): 
        if keyword in fname:                             
            print("im in 19")                                   
            PATH3 = os.path.join(PATH, fname) 
            return PATH3
    return "False"                 
####################################


##############################################   حذف فایل جیسون قدیمی(بروز بودن اطلاعات)
def delete_file(PATH):                       
    if find_file() != "False":               
        fname = find_file()
        print("im in 27")                  
        PATH2 = os.path.join(PATH, fname)    
        os.remove(PATH2)                     
##############################################


##############################################  تبدیل فایل جیسون به دیکشنری
def json_to_dict(fname):
    print(fname)
    print("im in 38")
    with open(fname) as json_file:
        data = json.load(json_file)
    return data
##############################################





############################################################################ ساخت کروم درایور پ دانلود فایل جیسون
def main1(PATH, PATH2):
    options = webdriver.ChromeOptions()                                   
    prefs = {"download.default_directory":PATH}
    options = webdriver.ChromeOptions()
    options.add_experimental_option("prefs", prefs)
    browser = webdriver.Chrome(executable_path = PATH2, chrome_options=options)
    delete_file(PATH)
    browser.get('https://www.kegg.jp/kegg-bin/download_htext?htext=br08402&format=json&filedir=')
    sleep(20)
    print("im in 53")
    browser.quit()
#####################################################################################################







########################################  لیست انواع بیماری ها
list_disses = [
                'cancers',
                'immune system diseases',
                'nervous system diseases',
                'cardiovascular diseases',
                'respiratory diseases',
                'endocrine and metabolic diseases',
                'digestive system diseases',
                'urinary system diseases',
                'reproductive system diseases',
                'musculoskeletal diseases',
                'skin diseases',
                'congenital disorders of metabolism',
                'congenital malformations',
                'other congenital disorders',
                'other diseases',
            ]
########################################


########################################    ذخیره اطلاعات در دیکشنری در صورت سرچ نوع بیماری
def make_big_dictionary():
    print("Im in 88")
    data = json_to_dict(find_file())
    data = data['children']
    print("im in 87")
    dict_data = {}
    counter = 0
    for i in data:
        dict_data[list_disses[counter]] = i['children']
        counter += 1
    return dict_data
##################################################




######################################## ساخت فایل اکسل در صورت سرچ نوع بیماری
def create_excel_file_big_data(data):  
    print(data)
    print("=============")
    workbook = xlsxwriter.Workbook(os.path.join(PATH, 'respnse.xlsx'))
    worksheet = workbook.add_worksheet()
    row = 0
    col = 0
    worksheet.write(row, col, 'code')
    worksheet.write(row, col+1, 'name')
    for i in data:
        row += 1
        i = i.split(' ')
        flag = 0
        x = ''
        for item in i:
            if flag == 0:
                flag = 1
                continue
            x += (' ' + item)

        worksheet.write(row, 0, str(i[0]))
        worksheet.write(row, 1, x)
    workbook.close()
    return workbook
###########################################         سرچ در فایل جیسون
def search(q):
    PATH = os.path.dirname(os.path.abspath(__file__)) #
    PATH2 = os.path.join(PATH, 'chromedriver')
    print("im in 118")
    main1(PATH, PATH2)
    if q.lower() in list_disses:         #######سرچ در صورت پیدا کردن نوع بیماری
        print("im in 124")
        dict_data = make_big_dictionary()
        list_dd = []
        for i in dict_data[q]:
            for item in i['children']:
                list_dd.append(item['name'])
        create_excel_file_big_data(list_dd)
#########################################################
    else:
                                            ####### سرچ در صورت وارد کردن کلید واژه خاص
        workbook = xlsxwriter.Workbook(os.path.join(PATH, 'respnse.xlsx'))
        worksheet = workbook.add_worksheet()
        col = 0
        worksheet.write(0, col, 'code')
        worksheet.write(0, col+1, 'name')
        worksheet.write(0, col+2, 'description')               ##ساخت فایل اکسل
        worksheet.write(0, col+3, 'category')
        worksheet.write(0, col+4, 'brite')
        PATH = os.path.dirname(os.path.abspath(__file__))
        path = os.path.join(PATH, 'br08402.json')
        f = open(path, 'r')
        row = 0
        for i in f:
            if q in i:
                i = i.strip("""\n\t""")
                i = i.strip()
                j = i.split(sep='"')
                x = j[3]
                y = x.split(' ')
                z = y[0]
                y.pop(0)
                y.pop(0)
                y = ' '.join(y)
                # print(z)
                # print(y)
                try:
                    options = webdriver.ChromeOptions()
                    prefs = {"download.default_directory":PATH}
                    options = webdriver.ChromeOptions()
                    options.add_experimental_option("prefs", prefs)
                    browser = webdriver.Chrome(executable_path = PATH2, chrome_options=options)
                    browser.set_window_position(-10000,0)
                    browser.get('https://www.kegg.jp/entry/ds:'+z)
                    name = browser.find_element_by_xpath("/html/body/div/table/tbody/tr/td/form/table/tbody/tr/td/table/tbody/tr[2]/td/div")
                    description = browser.find_element_by_xpath("/html/body/div/table/tbody/tr/td/form/table/tbody/tr/td/table/tbody/tr[3]/td/div")
                    category = browser.find_element_by_xpath("/html/body/div/table/tbody/tr/td/form/table/tbody/tr/td/table/tbody/tr[4]/td/div")
                    brite = browser.find_element_by_xpath("/html/body/div[1]/table/tbody/tr/td/form/table/tbody/tr/td/table/tbody/tr[5]/td/div/nobr")
                    row += 1
                    worksheet.write(row, col, z)           ### وب اسکرپینگ با سلنیوم و انتقال اطلاعات به اکسل
                    worksheet.write(row, col+1, name.text)
                    worksheet.write(row, col+2, description.text)
                    worksheet.write(row, col+3, category.text)
                    worksheet.write(row, col+4, brite.text)
                    browser.quit()
                except:
                    browser.quit()

        workbook.close()

